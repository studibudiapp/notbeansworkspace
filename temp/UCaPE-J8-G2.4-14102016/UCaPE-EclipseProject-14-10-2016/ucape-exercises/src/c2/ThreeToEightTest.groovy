package c2
  
// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel


import org.jcsp.lang.*
import org.jcsp.groovy.*

class ThreeToEightTest extends GroovyTestCase {
		
	void testResults() {		
		def connect1 = Channel.one2one()
		def connect2 = Channel.one2one()
		def generate =  new GenerateSetsOfThree ( outChannel: connect1.out() )
		def toStream =  new ListToStream ( inChannel: connect1.in(), 
                    		           outChannel: connect2.out() )
		def createEights = new CreateSetsOfEight ( inChannel: connect2.in() )
		
		def processList = [ generate, toStream, createEights ]
		new PAR (processList).run() 		
		def expected = [[1, 2, 3, 4, 5, 6, 7, 8], [9, 10, 11, 12, 13, 14, 15, 16], [17, 18, 19, 20, 21, 22, 23, 24]]		
		def actual = createEights.results		
		assertTrue(expected == actual)
	}
}
