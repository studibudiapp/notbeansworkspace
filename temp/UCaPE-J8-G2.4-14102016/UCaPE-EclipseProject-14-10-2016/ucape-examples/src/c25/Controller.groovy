package c25

import org.jcsp.awt.*
import org.jcsp.lang.*
import org.jcsp.util.*
import org.jcsp.groovy.*

class Controller implements CSProcess {
	int maxPlayers = 5
	
	void run(){
		def dList = new DisplayList()
		def gameCanvas = new ActiveCanvas()	
		gameCanvas.setPaintable(dList)	
		def statusConfig = Channel.createOne2One()
		def IPlabelConfig = Channel.createOne2One()
		def pairsConfig = Channel.createOne2One()
		def playerNames = Channel.createOne2One(maxPlayers)
		def pairsWon = Channel.createOne2One(maxPlayers)
		def playerNamesIn = new ChannelInputList(playerNames)
		def playerNamesOut = new ChannelOutputList(playerNames)
		def pairsWonIn = new ChannelInputList(pairsWon)
		def pairsWonOut = new ChannelOutputList(pairsWon)
// Create player update communications channels
		def gameStatusChannel = Channel.createOne2One()
		def gamePlayingChannel = Channel.createOne2One()
		def gameSpectatorsChannel = Channel.createOne2One()
		
		def network = [ new ControllerManager ( dList: dList,
												statusConfig: statusConfig.out(),
												IPlabelConfig: IPlabelConfig.out(),
												pairsConfig: pairsConfig.out(),
												playerNames: playerNamesOut,
												pairsWon: pairsWonOut,
												maxPlayers: maxPlayers,
// obtain game, player and spectator status
											   gameStatusOutput: gameStatusChannel.out(),
											   gamePlayingOutput: gamePlayingChannel.out(),
											   gameSpectatorsOutput: gameSpectatorsChannel.out()
											  ),
						new ControllerInterface( gameCanvas: gameCanvas,
												 statusConfig: statusConfig.in(),
												 IPlabelConfig: IPlabelConfig.in(),
												 pairsConfig: pairsConfig.in(),
												 playerNames: playerNamesIn,
												 pairsWon: pairsWonIn,
// Update game, player and spectator status
												 gameStatusInput: gameStatusChannel.in(),
												 gamePlayingInput: gamePlayingChannel.in(),
												 gameSpectatorsInput: gameSpectatorsChannel.in()
											   )
				  ]
		new PAR (network).run()
	}

}
